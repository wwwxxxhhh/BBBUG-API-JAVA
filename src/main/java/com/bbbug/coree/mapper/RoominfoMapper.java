package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Roominfo;
import com.bbbug.coree.entity.Roomuserinfo;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface RoominfoMapper {
    /**
     * 通过ID查询单条数据
     *
     * @param roomId 主键
     * @return 实例对象
     */
    @Select("select room_id, room_user, room_online, room_score, room_name, room_type, room_notice, room_addsong, room_sendmsg, room_robot, room_huya, room_reason, room_status, room_public, room_pwd ,room_background,admin ,room_playone,room_votepass,room_pushsongcd,room_addsongcd,room_addcount,room_pushdaycount from roominfo where room_id = #{roomId}")
    Roominfo queryById(Integer roomId);
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Select(" select room_id, room_user, room_online, room_score, room_name, room_type, room_notice, room_addsong, room_sendmsg, room_robot, room_huya, room_reason, room_status, room_public, room_pwd from roominfo limit #{offset}, #{limit} ")
    List<Roominfo> queryAllByLimit(int offset, int limit);
    @Select(" select room_id, room_user, room_online, room_score, room_name, room_type, room_notice,  room_status, room_public, admin,user_id,user_name,user_head from roominfo ,userinfo where room_user=user_id and room_status=0")
    List<Roomuserinfo> queryAll();
    /**
     * 新增数据
     *
     * @param roominfo 实例对象
     * @return 实例对象
     */
    @Insert("insert into roominfo(room_id,room_user,room_name, room_type, room_notice, room_status, room_public, admin)values (#{room_id}, #{room_user}, #{room_name}, #{room_type}, #{room_notice}, #{room_status}, #{room_public}, #{admin}) ")
    Boolean insert(Roominfo roominfo);

    /**
     * 修改数据
     *
     * @param roominfo 实例对象
     * @return 实例对象
     */
    @Update("update roominfo set room_type=#{room_type},room_background=#{room_background},room_public=#{room_public},room_name=#{room_name},room_notice=#{room_notice},room_pwd=#{room_pwd},room_status=#{room_status},room_robot=#{room_robot},room_sendmsg=#{room_sendmsg},room_addsongcd=#{room_addsongcd},room_pushsongcd=#{room_pushsongcd},room_addcount=#{room_addcount},room_playone=#{room_playone},room_votepass=#{room_votepass} ,room_addsong=#{room_addsong},room_pushdaycount=#{room_pushdaycount} where room_id=#{room_id}")
    Boolean update(Roominfo roominfo);

    /**
     * 通过主键删除数据
     *
     * @param roomId 主键
     * @return 是否成功
     */
    @Delete(" delete from roominfo where room_id = #{roomId}")
    boolean deleteById(Integer roomId);

    @Select("select room_id,room_robot from roominfo")
    List<Roominfo> queryAllsbrobot();

    @Select("select room_id,room_robot from roominfo where room_id=#{roomId}")
    Roominfo getroomneedsbronbotbyid(Integer roomId);

    @Select("select room_id from roominfo where admin=#{adminid}")
    List<Roominfo> queryByadmin(Integer adminid);
}
